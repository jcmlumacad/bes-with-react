import React from 'react';
import { render } from 'react-dom';
import Header from '#/layouts/header';
import Content from '#/layouts/content';
import Footer from '#/layouts/footer';

render(
    <div>
        <Header />
        <Content />
        <Footer />
    </div>,
    document.getElementById('root')
);

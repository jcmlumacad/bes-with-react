import React from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';

export default () => {
    return (
        <div className='mb-3'>
            <nav className='navbar navbar-expand-lg navbar-dark bg-dark'>
                <a className='navbar-brand' href='https://github.com/jcmlumacad' target='_new'>
                    <FontAwesomeIcon icon={['fab', 'github']} />&nbsp;jcmlumacad
                </a>
                {/* <button className='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarNavAltMarkup' aria-controls='navbarNavAltMarkup' aria-expanded='false' aria-label='Toggle navigation'>
                    <span className='navbar-toggler-icon' />
                </button>
                <div className='collapse navbar-collapse' id='navbarNavAltMarkup'>
                    <div className='navbar-nav'>
                        <a className='nav-item nav-link active' href='#'>Home <span className='sr-only'>(current)</span></a>
                        <a className='nav-item nav-link' href='#'>Repository</a>
                        <a className='nav-item nav-link' href='#'>Pricing</a>
                        <a className='nav-item nav-link disabled' href='#'>Disabled</a>
                    </div>
                </div> */}
            </nav>
        </div>
    );
};

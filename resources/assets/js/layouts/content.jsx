import _ from 'lodash';
import React, { Component } from 'react';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';

import Form from '#/libraries/Form';
import { timeSince } from '#/libraries/helpers';

class Content extends Component {
    constructor () {
        super();
        this.state = {
            repositories: []
        };
    }

    componentWillMount () {
        this.getRepositories();
    }

    getRepositories () {
        let query = `
            query ($number_of_repositories: Int!, $affiliations: [RepositoryAffiliation], $order_by: RepositoryOrder) {
                viewer {
                    name
                    repositories(last: $number_of_repositories, affiliations: $affiliations, orderBy: $order_by) {
                        nodes {
                            name
                            description
                            url
                            pushedAt
                        }
                    }
                }
            }
        `;
        let variables = {
            number_of_repositories: 10,
            affiliations: ['OWNER']
            // affiliations: ['OWNER', 'ORGANIZATION_MEMBER'],
            // order_by: {
            //     direction: 'ASC',
            //     field: 'PUSHED_AT'
            // }
        };

        let payload = { query, variables };
        let form = new Form(payload);
        form.post('graphql').then(result => {
            this.setRepositories(result.data.viewer.repositories.nodes);
        });
    }

    setRepositories (repositories) {
        this.setState({
            repositories
        });
    }

    render () {
        let chunkedRepositories = _.chunk(this.state.repositories, 3);
        let repositories = chunkedRepositories.map((repositories, index) => {
            let rowRepositories = repositories.map((repository, index) => {
                return (
                    <div className='card' key={index}>
                        <div className='card-body'>
                            <h5 className='card-title'>{repository.name}</h5>
                            <p className='card-text'>{repository.description || 'No description available'}</p>
                            <a href={repository.url} className='btn btn-primary btn-sm' target='_new'>
                                <FontAwesomeIcon icon={['fab', 'github']} />&nbsp;View in GitHub
                            </a>
                        </div>
                        <div className='card-footer'>
                            <small className='text-muted'>Last updated {timeSince(new Date(repository.pushedAt))} ago</small>
                        </div>
                    </div>
                );
            });

            return (
                <div className='row mb-3' key={index}>
                    <div className='card-deck'>
                        {rowRepositories}
                    </div>
                </div>
            );
        });

        return (
            <div className='container'>
                {repositories}
            </div>
        );
    }
}

export default Content;

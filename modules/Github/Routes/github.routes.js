/* global Route, modules, namespace */

export default () => {
    namespace(modules.github);

    Route.post('/graphql', 'GithubController@fetch', ['guest']);
};

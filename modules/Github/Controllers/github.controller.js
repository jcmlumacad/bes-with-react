/* global request, response, config */

import FormService from '~/modules/Shared/Services/FormService';

class GithubController {
    fetch () {
        let headersAttr = {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + config.graphql.github_token
            }
        };

        let payload = request.body;

        let form = new FormService(payload);

        form.post(config.graphql.github_api, headersAttr)
            .then(data => response.json(data))
            .catch(err => logger.error(err));
    }
}

export default GithubController;

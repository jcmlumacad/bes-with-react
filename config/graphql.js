/* global env */

export default {
    /**
     * Github Personal Token
     */
    github_token: env.GITHUB_PERSONAL_TOKEN,

    /**
     * GraphQL endpoint for Github
     */
    github_api: 'https://api.github.com/graphql'
};
